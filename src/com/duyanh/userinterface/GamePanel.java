/*
 * 1412008
 */
package com.duyanh.userinterface;

import com.duyanh.gameobject.GameWorld;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author Pisces
 */
public class GamePanel extends JPanel implements Runnable, KeyListener{
    
    private Thread thread;
    private boolean isRunning;
    private InputManager ipMng;
    private GameWorld gameWorld;
    
    public GamePanel() {
        gameWorld = new GameWorld();
        ipMng = new InputManager(gameWorld);
    }

    public void startGame() {
        if (thread == null) {
            isRunning = true;
            thread = new Thread(this);
            thread.start();
        }
    }
    
    @Override
    public void paint(Graphics g) {
        g.drawImage(gameWorld.getBufferedImage(), 0, 0, this);
    }
    
    @Override
    public void run() {
        final long MILLION = 1000000;
        long fps = 80;
        long period = 1000 * MILLION / fps;
        long beginTime = System.nanoTime();
        long sleepTime = 0;
        
        while(isRunning) {
            gameWorld.Update();
            gameWorld.Render();
            repaint();
            
            long deltaTime = System.nanoTime() - beginTime;
            sleepTime = period - deltaTime;
            try {
                if(sleepTime > 0) {
                    Thread.sleep(sleepTime/MILLION);
                }
                else {
                    Thread.sleep(period/(2*MILLION));
                }
            } catch (Exception e) {
            }
            beginTime = System.nanoTime();
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {}

    @Override
    public void keyPressed(KeyEvent e) {
        ipMng.processKeyPressed(e.getKeyCode());
    }

    @Override
    public void keyReleased(KeyEvent e) {
        ipMng.processKeyReleased(e.getKeyCode());
    }
    
}
