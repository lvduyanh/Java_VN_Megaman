/*
 * 1412008
 */
package com.duyanh.effect;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author Pisces
 */
public class FrameImage {
    private String name;
    private BufferedImage image;
    
    public FrameImage(String name, BufferedImage img) {
        this.name = name;
        this.image = img;
    }
    
    public FrameImage(FrameImage fImg) {
        image = new BufferedImage(fImg.getWidthImage(), fImg.getHeightImage(), fImg.getImage().getType());
        Graphics g = image.getGraphics();
        g.drawImage(fImg.getImage(), 0, 0, null);
        name = fImg.getName();
    }
    
    public void draw(Graphics2D g2, int x, int y) {
        g2.drawImage(image, x - image.getWidth()/2, y - image.getHeight()/2, null);
    }
    
    public FrameImage() {
        this.name = null;
        this.image = null;
    }
    
    public int getWidthImage() {
        return image.getWidth();
    }
    
    public int getHeightImage() {
        return image.getHeight();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BufferedImage getImage() {
        return image;
    }

    public void setImage(BufferedImage image) {
        this.image = image;
    }
    
}
